from django.http.response import JsonResponse

from app.internal.services.user_service import get_phone_number_or_none


def me_handler(request, user_id):
    phone_number = get_phone_number_or_none(user_id)
    phone_number = phone_number if phone_number is not None else 'Пользователь с выбранным id не зарегистрирован.'
    return JsonResponse({'phone_number': str(phone_number)})
