from telegram.ext import ConversationHandler

from app.internal.services.user_service import get_phone_number_or_none, check_phone_number, set_or_create_user


def start_handler(update, context):
    update.message.reply_text("Введите /set_phone для доступа ко всем функциям")


def set_phone_handler(update, context):
    update.message.reply_text("Введите номер телефона начиная с +")
    return 1


def me_handler(update, context):
    if phone := get_phone_number_or_none(update.effective_user.id) is not None:
        update.message.reply_text(str(phone))
    else:
        update.message.reply_text("Введите /set_phone для доступа ко всем функциям")


def add_phone_handler(update, context):
    text = update.message.text

    if check_phone_number(text):
        set_or_create_user(update.effective_user.id, text)
        update.message.reply_text("Регистрация успешна")
        return ConversationHandler.END
    else:
        update.message.reply_text("Неправильный номер, повторите ввод снова или введите /cancel для отмены")


def cancel_handler(update, context):
    return ConversationHandler.END
