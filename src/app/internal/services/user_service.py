from phonenumber_field.phonenumber import PhoneNumber

from app.models import TelegramUser


def get_phone_number_or_none(user_id):
    try:
        return TelegramUser.objects.get(telegram_id=user_id).phone_number
    except TelegramUser.DoesNotExist:
        return None


def set_or_create_user(user_id, phone_number):
    TelegramUser.objects.update_or_create(telegram_id=user_id, defaults={'phone_number': phone_number})


def check_phone_number(phone_number):
    return PhoneNumber.from_string(phone_number).is_valid()


