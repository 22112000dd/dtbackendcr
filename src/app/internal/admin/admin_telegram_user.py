from django.contrib import admin

from app.internal.models.telegram_user import TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    list_display = ('telegram_id', 'phone_number')
