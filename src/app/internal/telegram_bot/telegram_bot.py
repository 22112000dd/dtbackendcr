from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters
from app.internal.transport.bot import handlers

from config.settings import TELEGRAM_BOT_TOKEN


class TelegramBot:
    _updater: Updater = None

    def __init__(self, token):
        self._updater = Updater(
            token=token,
            use_context=True
        )

    def start_bot(self):
        self._updater.start_polling()
        self._updater.idle()

    def set_handlers(self):
        conv_handler = ConversationHandler(
            entry_points=[
                CommandHandler('start', handlers.start_handler),
                CommandHandler('set_phone', handlers.set_phone_handler),
                CommandHandler('me', handlers.me_handler)
            ],
            states={
                1: [MessageHandler(Filters.all, handlers.add_phone_handler),]
            },
            fallbacks=[CommandHandler('cancel', handlers.cancel_handler)],
            allow_reentry=True
        )
        self._updater.dispatcher.add_handler(conv_handler)


def start_bot():
    telegram_bot = TelegramBot(TELEGRAM_BOT_TOKEN)
    print("SET HANDLERS")
    telegram_bot.set_handlers()
    print("START")
    telegram_bot.start_bot()
