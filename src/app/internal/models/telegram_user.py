from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class TelegramUser(models.Model):

    telegram_id = models.IntegerField("Telegram", null=False)

    phone_number = PhoneNumberField(blank=True)

    def __str__(self):
        return f'{self.telegram_id}: {self.phone_number}'

    class Meta:
        db_table = 'telegram_users'
        verbose_name = 'Пользователь телеграм'
        verbose_name_plural = 'Пользователи телеграм'

